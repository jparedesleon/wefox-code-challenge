# Project setup

- **Typescript, React and SASS**: I decided to use these technologies as I have experience with them.
- **ESLint, Prettier and editorconfig**: The combination of these three results in a cross IDE, autoformatted code style guidelines which improve developer time and also code quality necessary for any type of project.
- **React-use-app**: This library abstracts all types of complex configuration (transpiling, linting, styling, and bundling) to start your project, since this is a basic project the library will be good enough to use.
- **React.useReducer + Context API**: In order to have a one-way data biding flow I've used a Redux like architecture architecture so I had to use both APIs to manage the state of App.

## What I really wanted to show:

- Own Redux like Architecture
- Modularization
- Single Responsability Principle
- Interface segregation Principle
- Dependency Inversion Principle
- Event-driven APP
- Best practices
- Typescript
- Code Common guideline
- Testing would be easy as we would test small pieces of funcionality isolated

## Acknowledgement:

- Handling Error network responses are not contemplated
- Missing Loading state on network request calls
- Missing Pagination
- Missing Virtual item rendering for large lists
- Missing Skeleton loading
- I would have liked to add my own CSS with BEM convention and ITCSS
- Missing Alert messages for network request confirmation
- Missing Maps to display lat and long
- Missing Unit testing
- There is a bit of code duplication that can be abstracted in the Controllers but the project is clear and harmless at this point.

## Projects where I've implemented the things I listed in the acknowledgement (Testing, CSS, Long lists):

- https://github.com/joseaplwork/music-player-react
- https://github.com/joseaplwork/million-books-list
- https://github.com/joseaplwork/music-album-typescript

# Start the project

**Before running the following script make sure you have docker running on your computer**

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

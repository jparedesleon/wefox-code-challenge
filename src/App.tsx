import React from 'react';

import { StateManager } from 'StateManager';
import { CitiesListController } from 'Controllers/CitiesListController';
import { DeleteCityController } from 'Controllers/DeleteCityController';
import { CreateCityController } from 'Controllers/CreateCityController';
import { EditCityController } from 'Controllers/EditCityController';
import { ViewCityController } from 'Controllers/ViewCityController';
import { Container } from 'Components/Container';
import { Header } from 'Components/Header';

import './App.scss';

function App() {
  return (
    <StateManager>
      <Header />
      <Container className="app__container">
        <CitiesListController />
        <DeleteCityController />
        <CreateCityController />
        <EditCityController />
        <ViewCityController />
      </Container>
    </StateManager>
  );
}

export default App;

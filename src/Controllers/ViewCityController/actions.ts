import { Action } from 'StateManager';
import { City } from 'Model';

import * as C from './constants';

export type ToggleViewCityFormVisibilityPayload = { city?: City };
export type ToggleViewCityFormVisibilityAction = Action<
  typeof C.TOGGLE_VIEW_CITY_FORM_VISIBILITY,
  ToggleViewCityFormVisibilityPayload
>;
export function toggleViewCityFormVisibility(
  city?: City,
): ToggleViewCityFormVisibilityAction {
  return {
    type: C.TOGGLE_VIEW_CITY_FORM_VISIBILITY,
    payload: { city },
  };
}

export type Actions = ToggleViewCityFormVisibilityAction;

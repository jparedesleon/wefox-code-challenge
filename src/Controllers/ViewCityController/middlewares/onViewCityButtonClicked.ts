import { Actions, Dispatcher, SYSTEM_MESSAGE } from 'StateManager';
import { City } from 'Model';

import { toggleViewCityFormVisibility } from '../actions';

export function onViewCityButtonClicked(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'VIEW_CITY_BUTTON_CLICKED'
  ) {
    const { data } = action.payload;
    dispatch(toggleViewCityFormVisibility(data as City));
  }
}

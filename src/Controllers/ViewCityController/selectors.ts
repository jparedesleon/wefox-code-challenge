import { State } from 'StateManager';
import { City } from 'Model';

export const selectViewCityController = (state: State) => state.viewCityController;

export const selectFormVisibility = (state: State): boolean => {
  const localState = selectViewCityController(state);

  return localState.openForm;
};

export const selectCity = (state: State): City | null => {
  const localState = selectViewCityController(state);

  return localState.city;
};

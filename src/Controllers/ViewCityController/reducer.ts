import { Actions } from 'StateManager';
import { City } from 'Model';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type ViewCityControllerState = {
  city: City | null;
  openForm: boolean;
};

export type ViewCityControllerActions = LocalActions;
export const initialState: ViewCityControllerState = {
  city: null,
  openForm: false,
};

export function reducer(state = initialState, action: Actions): ViewCityControllerState {
  switch (action.type) {
    case C.TOGGLE_VIEW_CITY_FORM_VISIBILITY: {
      const { city } = action.payload!;

      return {
        ...state,
        openForm: !state.openForm,
        city: city || state.city,
      };
    }
    default: {
      return state;
    }
  }
}

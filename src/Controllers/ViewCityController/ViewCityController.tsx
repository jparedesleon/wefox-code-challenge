import React, { useContext } from 'react';

import { StateContext } from 'StateManager';
import { CityCardView } from 'Components/CityCardView';
import { Dialog } from 'Components/Dialog';

import { toggleViewCityFormVisibility } from './actions';
import { selectFormVisibility, selectCity } from './selectors';

export function ViewCityController() {
  const { state, dispatch } = useContext(StateContext);
  const isFormVisible = selectFormVisibility(state);
  const city = selectCity(state);

  const handleFormVisibility = () => {
    dispatch(toggleViewCityFormVisibility());
  };
  return (
    <Dialog open={isFormVisible} onClose={handleFormVisibility} noWrap>
      {city && <CityCardView city={city} onClick={handleFormVisibility} />}
    </Dialog>
  );
}

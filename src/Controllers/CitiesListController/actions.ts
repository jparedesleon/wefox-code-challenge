import { Action } from 'StateManager';
import { CitiesJson, City, CityJson } from 'Model';

import * as C from './constants';

export type FetchCitiesAction = Action<typeof C.GET_CITIES_REQUEST>;
export function fetchCities(): FetchCitiesAction {
  return { type: C.GET_CITIES_REQUEST };
}

export type FetchCitiesSuccessAction = Action<
  typeof C.GET_CITIES_REQUEST_SUCCESS,
  CitiesJson
>;
export function fetchCitiesSuccess(data: CitiesJson): FetchCitiesSuccessAction {
  return {
    type: C.GET_CITIES_REQUEST_SUCCESS,
    payload: data,
  };
}

export type RemoveFromCitiesListPayload = { id: City['id'] };
export type RemoveFromCitiesListAction = Action<
  typeof C.REMOVE_FROM_CITIES_LIST,
  RemoveFromCitiesListPayload
>;
export function removeFromCitiesList(id: City['id']): RemoveFromCitiesListAction {
  return {
    type: C.REMOVE_FROM_CITIES_LIST,
    payload: {
      id,
    },
  };
}

export type AddToCitiesListPayload = { city: CityJson };
export type AddToCitiesListAction = Action<
  typeof C.ADD_TO_CITIES_LIST,
  AddToCitiesListPayload
>;
export function addToCitiesList(city: CityJson): AddToCitiesListAction {
  return {
    type: C.ADD_TO_CITIES_LIST,
    payload: {
      city,
    },
  };
}

export type UpdateCitiesListPayload = { city: CityJson };
export type UpdateCitiesListAction = Action<
  typeof C.UPDATE_TO_CITIES_LIST,
  UpdateCitiesListPayload
>;
export function updateCitiesList(city: CityJson): UpdateCitiesListAction {
  return {
    type: C.UPDATE_TO_CITIES_LIST,
    payload: {
      city,
    },
  };
}

export type Actions =
  | FetchCitiesAction
  | FetchCitiesSuccessAction
  | AddToCitiesListAction
  | UpdateCitiesListAction
  | RemoveFromCitiesListAction;

import React, { useContext, useEffect } from 'react';

import { emitSystemMessage, StateContext } from 'StateManager';
import { CitiesDataTable } from 'Components/CitiesDataTable';
import { City } from 'Model';

import { fetchCities } from './actions';
import { selectCities, selectCity } from './selectors';

export function CitiesListController() {
  const { state, dispatch } = useContext(StateContext);
  const cities = selectCities(state);
  const handleEditCity = (id: City['id']) => {
    dispatch(
      emitSystemMessage({
        message: 'EDIT_CITY_BUTTON_CLICKED',
        data: selectCity(state, id),
      }),
    );
  };
  const handleDeleteCity = (id: City['id']) => {
    dispatch(
      emitSystemMessage({
        message: 'DELETE_CITY_BUTTON_CLICKED',
        data: id,
      }),
    );
  };
  const handleViewCity = (id: City['id']) => {
    dispatch(
      emitSystemMessage({
        message: 'VIEW_CITY_BUTTON_CLICKED',
        data: selectCity(state, id),
      }),
    );
  };

  useEffect(() => {
    dispatch(fetchCities());
  }, []);

  return (
    <CitiesDataTable
      cities={cities}
      onEditCity={handleEditCity}
      onDeleteCity={handleDeleteCity}
      onViewCity={handleViewCity}
    />
  );
}

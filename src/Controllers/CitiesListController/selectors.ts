import { State } from 'StateManager';
import { Cities, City } from 'Model';

export const selectCitieslistController = (state: State) => state.citiesListController;

export const selectCities = (state: State): Cities => {
  const citiesList = selectCitieslistController(state);

  return citiesList.cities;
};

export const selectCity = (state: State, id: City['id']): City | undefined => {
  const citiesList = selectCitieslistController(state);

  return citiesList.cities.find((city) => city.id === id);
};

import { Actions } from 'StateManager';
import { Cities, transformCity } from 'Model';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type CitiesListControllerState = {
  cities: Cities;
};

export type CitiesListControllerActions = LocalActions;
export const initialState: CitiesListControllerState = {
  cities: [],
};

export function reducer(
  state = initialState,
  action: Actions,
): CitiesListControllerState {
  switch (action.type) {
    case C.GET_CITIES_REQUEST_SUCCESS: {
      const data = action.payload!;

      return {
        ...state,
        cities: data.map(transformCity),
      };
    }
    case C.REMOVE_FROM_CITIES_LIST: {
      return {
        ...state,
        cities: state.cities.filter((city) => city.id !== action.payload?.id),
      };
    }
    case C.ADD_TO_CITIES_LIST: {
      return {
        ...state,
        cities: [...state.cities, transformCity(action.payload?.city!)],
      };
    }
    case C.UPDATE_TO_CITIES_LIST: {
      return {
        ...state,
        cities: state.cities.map((city) => {
          if (city.id === action.payload?.city.id) {
            return transformCity(action.payload?.city);
          }

          return city;
        }),
      };
    }
    default: {
      return state;
    }
  }
}

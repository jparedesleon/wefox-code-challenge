import { Dispatcher, Actions } from 'StateManager';
import { NetworkRequest } from 'utils/NetworkRequest';

import { fetchCitiesSuccess } from '../actions';
import * as C from '../constants';

export async function onRequestCities(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.GET_CITIES_REQUEST) {
    const requester = new NetworkRequest('http://localhost:3000/api/v1/posts');

    const request = await requester.sendRequest();

    const data = await request.json();

    dispatch(fetchCitiesSuccess(data));
  }
}

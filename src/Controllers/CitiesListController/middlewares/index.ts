import { onCityDeleted } from './onCityDeleted';
import { onCityCreated } from './onCityCreated';
import { onCityEdited } from './onCityEdited';
import { onRequestCities } from './onRequestCities';

export const middlewares = [onRequestCities, onCityDeleted, onCityCreated, onCityEdited];

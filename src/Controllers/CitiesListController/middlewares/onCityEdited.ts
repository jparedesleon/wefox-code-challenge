import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { CityJson } from 'Model';

import { updateCitiesList } from '../actions';

export async function onCityEdited(action: Actions, dispatch: Dispatcher) {
  if (action.type === SYSTEM_MESSAGE && action.payload?.message === 'CITY_EDITED') {
    const { data } = action.payload;

    dispatch(updateCitiesList(data as CityJson));
  }
}

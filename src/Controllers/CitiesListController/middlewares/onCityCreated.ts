import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { CityJson } from 'Model';

import { addToCitiesList } from '../actions';

export async function onCityCreated(action: Actions, dispatch: Dispatcher) {
  if (action.type === SYSTEM_MESSAGE && action.payload?.message === 'CITY_CREATED') {
    const { data } = action.payload;

    dispatch(addToCitiesList(data as CityJson));
  }
}

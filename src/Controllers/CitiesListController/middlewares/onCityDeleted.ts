import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { City } from 'Model';

import { removeFromCitiesList } from '../actions';

export async function onCityDeleted(action: Actions, dispatch: Dispatcher) {
  if (action.type === SYSTEM_MESSAGE && action.payload?.message === 'CITY_DELETED') {
    const { data } = action.payload;

    dispatch(removeFromCitiesList(data as City['id']));
  }
}

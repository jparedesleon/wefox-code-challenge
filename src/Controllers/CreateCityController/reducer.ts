import { Actions } from 'StateManager';
import { CityJson, FormDataList } from 'Model';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type CreateCityFormData = FormDataList<
  keyof Omit<CityJson, 'id' | 'created_at' | 'updated_at'>
>;
export type CreateCityControllerState = {
  city: Partial<Omit<CityJson, 'id' | 'created_at' | 'updated_at'>>;
  cityFormData: CreateCityFormData;
  openForm: boolean;
  canSubmit: boolean;
};

export type CreateCityControllerActions = LocalActions;
const intialFormData = [
  { type: 'input', label: 'Title', name: 'title' as const, value: '' },
  { type: 'textarea', label: 'Content', name: 'content' as const, value: '' },
  { type: 'input', label: 'Image URL', name: 'image_url' as const, value: '' },
  { type: 'number', label: 'Latitude', name: 'lat' as const, value: '' },
  { type: 'number', label: 'Longitude', name: 'long' as const, value: '' },
];
export const initialState: CreateCityControllerState = {
  cityFormData: intialFormData,
  city: {},
  openForm: false,
  canSubmit: false,
};

export function reducer(
  state = initialState,
  action: Actions,
): CreateCityControllerState {
  switch (action.type) {
    case C.TOGGLE_CREATE_CITY_FORM_VISIBILITY: {
      return {
        ...state,
        openForm: !state.openForm,
      };
    }
    case C.REQUEST_CREATE_CITY_SUCCES: {
      return {
        cityFormData: intialFormData,
        city: {},
        openForm: false,
        canSubmit: false,
      };
    }
    case C.FIELD_CHANGE: {
      const { value, name } = action.payload!;
      const newState = {
        ...state,
        city: {
          ...state.city,
          [name]: value,
        },
        cityFormData: state.cityFormData.map((data) => {
          if (data.name === name) {
            return {
              ...data,
              value,
            };
          }

          return data;
        }),
      };

      return {
        ...newState,
        canSubmit: !!newState.city.title && !!newState.city.content,
      };
    }
    default: {
      return state;
    }
  }
}

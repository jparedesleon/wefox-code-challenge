import { Action } from 'StateManager';
import { City } from 'Model';

import * as C from './constants';

export type ToggleCreateCityFormVisibilityAction = Action<
  typeof C.TOGGLE_CREATE_CITY_FORM_VISIBILITY
>;
export function toggleCreateCityFormVisibility(): ToggleCreateCityFormVisibilityAction {
  return {
    type: C.TOGGLE_CREATE_CITY_FORM_VISIBILITY,
  };
}
export type RequestCreateCityPayload = { city: City };
export type RequestCreateCityAction = Action<
  typeof C.REQUEST_CREATE_CITY,
  RequestCreateCityPayload
>;
export function requestCreateCity(city: City): RequestCreateCityAction {
  return {
    type: C.REQUEST_CREATE_CITY,
    payload: { city },
  };
}

export type RequestCreateCitySuccessAction = Action<typeof C.REQUEST_CREATE_CITY_SUCCES>;
export function requestCreateSuccessCity(): RequestCreateCitySuccessAction {
  return {
    type: C.REQUEST_CREATE_CITY_SUCCES,
  };
}
export type FieldChangePayload = { value: string; name: string };
export type FieldChangeAction = Action<typeof C.FIELD_CHANGE, FieldChangePayload>;
export function fieldChange(value: string, name: string): FieldChangeAction {
  return {
    type: C.FIELD_CHANGE,
    payload: {
      value,
      name,
    },
  };
}

export type Actions =
  | RequestCreateCityAction
  | RequestCreateCitySuccessAction
  | FieldChangeAction
  | ToggleCreateCityFormVisibilityAction;

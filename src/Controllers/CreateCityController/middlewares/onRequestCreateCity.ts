import { Actions, Dispatcher, emitSystemMessage } from 'StateManager';
import { NetworkRequest } from 'utils/NetworkRequest';

import { requestCreateSuccessCity } from '../actions';
import * as C from '../constants';

export async function onRequestCreateCity(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.REQUEST_CREATE_CITY) {
    const requester = new NetworkRequest('http://localhost:3000/api/v1/posts', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: action.payload?.city,
    });

    const request = await requester.sendRequest();

    const data = await request.json();

    dispatch(emitSystemMessage({ message: 'CITY_CREATED', data }));
    dispatch(requestCreateSuccessCity());
  }
}

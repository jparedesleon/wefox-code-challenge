import React, { useContext } from 'react';
import { City } from 'Model';
import { StateContext } from 'StateManager';
import { FormDialog } from 'Components/FormDialog';
import { AddFloatingButton } from 'Components/AddFloatingButton';

import {
  requestCreateCity,
  toggleCreateCityFormVisibility,
  fieldChange,
} from './actions';
import {
  selectFormVisibility,
  selectCity,
  selectCanSubmit,
  selectFormData,
} from './selectors';
import './CreateCityController.scss';

export function CreateCityController() {
  const { state, dispatch } = useContext(StateContext);
  const isFormVisible = selectFormVisibility(state);
  const canSubmit = selectCanSubmit(state);
  const data = selectFormData(state);
  const city = selectCity(state);

  const handleFormVisibility = () => {
    dispatch(toggleCreateCityFormVisibility());
  };
  const handleCreateCity = () => {
    dispatch(requestCreateCity(city as City));
  };
  const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;

    dispatch(fieldChange(value, name));
  };

  return (
    <>
      <div className="create-city__floating-button">
        <AddFloatingButton onClick={handleFormVisibility} />
      </div>
      <FormDialog
        open={isFormVisible}
        disableSubmit={!canSubmit}
        onFieldChange={handleFieldChange}
        onClose={handleFormVisibility}
        onSubmit={handleCreateCity}
        cancelText="Cancel"
        submitText="Create"
        title="Create City"
        data={data}
      />
    </>
  );
}

import { State } from 'StateManager';
import { CityJson } from 'Model';

import { CreateCityFormData } from './reducer';

export const selectCreateCityController = (state: State) => state.createCityController;

export const selectFormVisibility = (state: State): boolean => {
  const localState = selectCreateCityController(state);

  return localState.openForm;
};

export const selectFormData = (state: State): CreateCityFormData => {
  const localState = selectCreateCityController(state);

  return localState.cityFormData;
};

export const selectCity = (state: State): Omit<Partial<CityJson>, 'id'> => {
  const localState = selectCreateCityController(state);

  return localState.city;
};

export const selectCanSubmit = (state: State): boolean => {
  const localState = selectCreateCityController(state);

  return localState.canSubmit;
};

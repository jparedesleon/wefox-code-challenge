import { State } from 'StateManager';
import { CityJson } from 'Model';

import { EditCityFormData } from './reducer';

export const selectEditCityController = (state: State) => state.editCityController;

export const selectFormVisibility = (state: State): boolean => {
  const localState = selectEditCityController(state);

  return localState.openForm;
};

export const selectCity = (state: State): Partial<CityJson> | null => {
  const localState = selectEditCityController(state);

  return localState.city;
};

export const selectFormData = (state: State): EditCityFormData => {
  const localState = selectEditCityController(state);

  return localState.cityFormData;
};

export const selectCanSubmit = (state: State): boolean => {
  const localState = selectEditCityController(state);

  return localState.canSubmit;
};

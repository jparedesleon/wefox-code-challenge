import React, { useContext } from 'react';

import { StateContext } from 'StateManager';
import { City } from 'Model';
import { FormDialog } from 'Components/FormDialog';

import { requestEditCity, toggleEditCityFormVisibility, fieldChange } from './actions';
import {
  selectFormVisibility,
  selectCity,
  selectCanSubmit,
  selectFormData,
} from './selectors';

export function EditCityController() {
  const { state, dispatch } = useContext(StateContext);
  const isFormVisible = selectFormVisibility(state);
  const canSubmit = selectCanSubmit(state);
  const data = selectFormData(state);
  const city = selectCity(state);

  const handleFormVisibility = () => {
    dispatch(toggleEditCityFormVisibility());
  };
  const handleEditCity = () => {
    dispatch(requestEditCity(city as City));
  };
  const handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;

    dispatch(fieldChange(value, name));
  };

  return (
    <FormDialog
      open={isFormVisible}
      disableSubmit={!canSubmit}
      onFieldChange={handleFieldChange}
      onClose={handleFormVisibility}
      onSubmit={handleEditCity}
      cancelText="Cancel"
      submitText="Update"
      title="Update City"
      data={data}
    />
  );
}

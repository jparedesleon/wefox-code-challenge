import { Action } from 'StateManager';
import { City } from 'Model';

import * as C from './constants';

export type ToggleEditCityFormVisibilityPayload = { city?: City };
export type ToggleEditCityFormVisibilityAction = Action<
  typeof C.TOGGLE_EDIT_CITY_FORM_VISIBILITY,
  ToggleEditCityFormVisibilityPayload
>;
export function toggleEditCityFormVisibility(
  city?: City,
): ToggleEditCityFormVisibilityAction {
  return {
    type: C.TOGGLE_EDIT_CITY_FORM_VISIBILITY,
    payload: { city },
  };
}

export type RequestEditCityPayload = { city: Partial<City> };
export type RequestEditCityAction = Action<
  typeof C.REQUEST_EDIT_CITY,
  RequestEditCityPayload
>;
export function requestEditCity(city: Partial<City>): RequestEditCityAction {
  return {
    type: C.REQUEST_EDIT_CITY,
    payload: { city },
  };
}

export type RequestEditCitySuccessAction = Action<typeof C.REQUEST_EDIT_CITY_SUCCES>;
export function requestEdtiSuccessCity(): RequestEditCitySuccessAction {
  return {
    type: C.REQUEST_EDIT_CITY_SUCCES,
  };
}

export type FieldChangePayload = { value: string; name: string };
export type FieldChangeAction = Action<typeof C.FIELD_CHANGE, FieldChangePayload>;
export function fieldChange(value: string, name: string): FieldChangeAction {
  return {
    type: C.FIELD_CHANGE,
    payload: {
      value,
      name,
    },
  };
}

export type Actions =
  | RequestEditCityAction
  | RequestEditCitySuccessAction
  | FieldChangeAction
  | ToggleEditCityFormVisibilityAction;

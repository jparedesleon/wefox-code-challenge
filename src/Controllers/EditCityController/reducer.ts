import { Actions } from 'StateManager';
import { CityJson, FormDataList } from 'Model';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type EditCityFormData = FormDataList<
  keyof Omit<CityJson, 'id' | 'created_at' | 'updated_at'>
>;
export type EditCityControllerState = {
  cityFormData: EditCityFormData;
  city: Partial<CityJson> | null;
  openForm: boolean;
  canSubmit: boolean;
};

export type EditCityControllerActions = LocalActions;
const intialFormData = [
  { type: 'input', label: 'Title', name: 'title' as const, value: '' },
  { type: 'textarea', label: 'Content', name: 'content' as const, value: '' },
  { type: 'input', label: 'Image URL', name: 'image_url' as const, value: '' },
  { type: 'number', label: 'Latitude', name: 'lat' as const, value: '' },
  { type: 'number', label: 'Longitude', name: 'long' as const, value: '' },
];
export const initialState: EditCityControllerState = {
  cityFormData: intialFormData,
  city: null,
  openForm: false,
  canSubmit: false,
};

export function reducer(state = initialState, action: Actions): EditCityControllerState {
  switch (action.type) {
    case C.TOGGLE_EDIT_CITY_FORM_VISIBILITY: {
      const { city } = action.payload!;
      const cityJson = city && {
        id: city.id,
        title: city.title,
        content: city.content,
        image_url: city.imageUrl,
        lat: city.lat?.toString(),
        long: city?.long?.toString(),
      };

      const newCityFormData = state.cityFormData.map((data) => {
        if (cityJson) {
          return {
            ...data,
            value: cityJson[data.name],
          };
        }

        return data;
      });

      return {
        ...state,
        openForm: !state.openForm,
        city: cityJson || state.city,
        cityFormData: newCityFormData,
        canSubmit: !!city?.title && !!city.content,
      };
    }
    case C.REQUEST_EDIT_CITY_SUCCES: {
      return {
        cityFormData: intialFormData,
        city: null,
        openForm: false,
        canSubmit: false,
      };
    }
    case C.FIELD_CHANGE: {
      const { value, name } = action.payload!;
      const newState = {
        ...state,
        city: {
          ...state.city,
          [name]: value,
        } as CityJson,
        cityFormData: state.cityFormData.map((data) => {
          if (data.name === name) {
            return {
              ...data,
              value,
            };
          }

          return data;
        }),
      };

      return {
        ...newState,
        canSubmit: !!newState.city.title && !!newState.city.content,
      };
    }
    default: {
      return state;
    }
  }
}

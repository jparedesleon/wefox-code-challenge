import { Actions, Dispatcher, emitSystemMessage } from 'StateManager';
import { CityJson } from 'Model';
import { NetworkRequest } from 'utils/NetworkRequest';

import { requestEdtiSuccessCity } from '../actions';
import * as C from '../constants';

export async function onRequestEditCity(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.REQUEST_EDIT_CITY) {
    const { city } = action.payload!;
    const requester = new NetworkRequest(
      `http://localhost:3000/api/v1/posts/${city.id}`,
      {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: city,
      },
    );

    const request = await requester.sendRequest();
    const data = await request.json();

    dispatch(emitSystemMessage({ message: 'CITY_EDITED', data: data as CityJson }));
    dispatch(requestEdtiSuccessCity());
  }
}

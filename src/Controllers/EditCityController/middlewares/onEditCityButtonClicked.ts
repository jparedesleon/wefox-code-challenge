import { Actions, Dispatcher, SYSTEM_MESSAGE } from 'StateManager';
import { City } from 'Model';

import { toggleEditCityFormVisibility } from '../actions';

export function onEditCityButtonClicked(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'EDIT_CITY_BUTTON_CLICKED'
  ) {
    const { data } = action.payload;
    dispatch(toggleEditCityFormVisibility(data as City));
  }
}

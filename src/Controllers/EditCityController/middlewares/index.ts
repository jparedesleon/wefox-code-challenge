import { onRequestEditCity } from './onRequestEditCity';
import { onEditCityButtonClicked } from './onEditCityButtonClicked';

export const middlewares = [onRequestEditCity, onEditCityButtonClicked];

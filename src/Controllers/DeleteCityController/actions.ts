import { Action } from 'StateManager';
import { City } from 'Model';

import * as C from './constants';

export type DeleteCityConfirmationPayload = {
  id: City['id'];
};
export type DeleteCityConfirmationAction = Action<
  typeof C.DELETE_CITY_CONFIRMATION,
  DeleteCityConfirmationPayload
>;
export function deleteCityConfirmation(id: City['id']): DeleteCityConfirmationAction {
  return {
    type: C.DELETE_CITY_CONFIRMATION,
    payload: {
      id,
    },
  };
}

export type CancelDeleteCityAction = Action<typeof C.CANCEL_DELETE_CITY>;
export function cancelDeleteCity(): CancelDeleteCityAction {
  return {
    type: C.CANCEL_DELETE_CITY,
  };
}

export type RequestDeleteCityAction = Action<
  typeof C.REQUEST_DELETE_CITY,
  DeleteCityConfirmationPayload
>;
export function requestDeleteCity(id: City['id']): RequestDeleteCityAction {
  return {
    type: C.REQUEST_DELETE_CITY,
    payload: {
      id,
    },
  };
}

export type RequestDeleteCitySuccessAction = Action<typeof C.REQUEST_DELETE_CITY_SUCCES>;
export function requestDeleteSuccessCity(): RequestDeleteCitySuccessAction {
  return {
    type: C.REQUEST_DELETE_CITY_SUCCES,
  };
}

export type Actions =
  | DeleteCityConfirmationAction
  | CancelDeleteCityAction
  | RequestDeleteCityAction
  | RequestDeleteCitySuccessAction;

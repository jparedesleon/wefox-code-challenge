import React, { useContext } from 'react';

import { StateContext } from 'StateManager';
import { Button } from 'Components/Button';
import { Dialog } from 'Components/Dialog';

import { selectCityToDelete } from './selectors';
import { requestDeleteCity, cancelDeleteCity } from './actions';

const DialogActions = ({
  handleClose,
  handleDelete,
}: {
  handleClose: () => void;
  handleDelete: () => void;
}) => (
  <>
    <Button onClick={handleClose} color="primary">
      Keep
    </Button>
    <Button onClick={handleDelete} color="secondary">
      Delete
    </Button>
  </>
);

export function DeleteCityController() {
  const { state, dispatch } = useContext(StateContext);
  const cityId = selectCityToDelete(state);

  const handleClose = () => {
    dispatch(cancelDeleteCity());
  };

  const handleDelete = () => {
    if (cityId) {
      dispatch(requestDeleteCity(cityId));
    }
  };

  return (
    <Dialog
      open={cityId !== null}
      onClose={handleClose}
      title="Delete city?"
      actions={<DialogActions handleClose={handleClose} handleDelete={handleDelete} />}
    >
      Are you sure you want to delete this city?
    </Dialog>
  );
}

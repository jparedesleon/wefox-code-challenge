import { Actions, Dispatcher, emitSystemMessage } from 'StateManager';
import { NetworkRequest } from 'utils/NetworkRequest';

import { DeleteCityConfirmationPayload, requestDeleteSuccessCity } from '../actions';
import * as C from '../constants';

export async function onRequestDeleteCity(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.REQUEST_DELETE_CITY) {
    const { id } = action.payload as DeleteCityConfirmationPayload;

    const requester = new NetworkRequest(`http://localhost:3000/api/v1/posts/${id}`, {
      method: 'DELETE',
    });

    await requester.sendRequest();

    dispatch(emitSystemMessage({ message: 'CITY_DELETED', data: id }));
    dispatch(requestDeleteSuccessCity());
  }
}

import { onDeleteCityButtonClicked } from './onDeleteCityButtonClicked';
import { onRequestDeleteCity } from './onRequestDeleteCity';

export const middlewares = [onRequestDeleteCity, onDeleteCityButtonClicked];

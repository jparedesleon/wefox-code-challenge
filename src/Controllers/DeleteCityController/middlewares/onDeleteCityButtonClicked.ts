import { Actions, Dispatcher, SYSTEM_MESSAGE } from 'StateManager';
import { City } from 'Model';

import { deleteCityConfirmation } from '../actions';

export function onDeleteCityButtonClicked(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'DELETE_CITY_BUTTON_CLICKED'
  ) {
    dispatch(deleteCityConfirmation(action.payload.data as City['id']));
  }
}

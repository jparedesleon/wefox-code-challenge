import { State } from 'StateManager';
import { City } from 'Model';

export const selectDeleteCityController = (state: State) => state.deleteCityController;

export const selectCityToDelete = (state: State): City['id'] | null => {
  const localState = selectDeleteCityController(state);

  return localState.cityId;
};

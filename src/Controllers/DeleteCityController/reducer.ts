import { Actions } from 'StateManager';
import { City } from 'Model';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type DeleteCityControllerState = {
  cityId: City['id'] | null;
};

export type DeleteCityControllerActions = LocalActions;
export const initialState: DeleteCityControllerState = {
  cityId: null,
};

export function reducer(
  state = initialState,
  action: Actions,
): DeleteCityControllerState {
  switch (action.type) {
    case C.DELETE_CITY_CONFIRMATION: {
      return {
        cityId: action.payload?.id || null,
      };
    }
    case C.CANCEL_DELETE_CITY:
    case C.REQUEST_DELETE_CITY_SUCCES: {
      return {
        cityId: null,
      };
    }
    default: {
      return state;
    }
  }
}

import { City, CityJson } from './model';

export function transformCity(city: CityJson): City {
  return {
    id: city.id,
    title: city.title,
    content: city.content,
    imageUrl: city.image_url,
    lat: city.lat ? parseFloat(city.lat) : undefined,
    long: city.long ? parseFloat(city.long) : undefined,
    createdAt: city.created_at ? new Date(city.created_at) : undefined,
    updatedAt: city.updated_at ? new Date(city.updated_at) : undefined,
  };
}

export type City = {
  id: number;
  title: string;
  content: string;
  lat?: number;
  long?: number;
  imageUrl?: string;
  createdAt?: Date;
  updatedAt?: Date;
};

export type Cities = Array<City>;

export type CityJson = {
  id: number;
  title: string;
  content: string;
  lat?: string;
  long?: string;
  // eslint-disable-next-line camelcase
  image_url?: string;
  // eslint-disable-next-line camelcase
  created_at?: string;
  // eslint-disable-next-line camelcase
  updated_at?: Date;
};

export type CitiesJson = Array<CityJson>;

export type FormData<N extends string> = {
  type: string;
  label: string;
  name: N;
  value: any;
};
export type FormDataList<N extends string> = Array<FormData<N>>;

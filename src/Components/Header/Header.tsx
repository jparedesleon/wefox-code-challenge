import React from 'react';

import { LogoIcon } from 'Components/Icons';

import './Header.scss';

export function Header() {
  return (
    <header className="Header">
      <LogoIcon />
      <h1 className="Header__title">Cities app</h1>
    </header>
  );
}

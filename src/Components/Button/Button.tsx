import React from 'react';

import ButtonMUI from '@material-ui/core/Button';

type Props = {
  size?: 'small' | 'medium' | 'large';
  color?: 'primary' | 'secondary';
  disabled?: boolean;
  onClick?: () => void;
};

export function Button({
  children,
  onClick,
  color,
  disabled,
  size,
}: React.PropsWithChildren<Props>) {
  return (
    <ButtonMUI onClick={onClick} size={size} color={color} disabled={disabled}>
      {children}
    </ButtonMUI>
  );
}

import React from 'react';

import TextField from '@material-ui/core/TextField';

import { Dialog } from 'Components/Dialog';
import { Button } from 'Components/Button';
import { FormDataList } from 'Model';

type Props<N extends string> = {
  open: boolean;
  disableSubmit: boolean;
  cancelText: string;
  submitText: string;
  title: string;
  data: FormDataList<N>;
  onClose: () => void;
  onSubmit: () => void;
  onFieldChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const FormActions = <N extends string>(
  props: Pick<
    Props<N>,
    'onClose' | 'disableSubmit' | 'onSubmit' | 'cancelText' | 'submitText'
  >,
) => {
  const { onClose, disableSubmit, onSubmit, cancelText, submitText } = props;

  return (
    <>
      <Button onClick={onClose} color="secondary">
        {cancelText}
      </Button>
      <Button disabled={disableSubmit} onClick={onSubmit} color="primary">
        {submitText}
      </Button>
    </>
  );
};

export function FormDialog<N extends string>(props: Props<N>) {
  const {
    open,
    onFieldChange,
    onClose,
    disableSubmit,
    onSubmit,
    cancelText,
    submitText,
    title,
    data,
  } = props;

  return (
    <Dialog
      title={title}
      open={open}
      onClose={onClose}
      actions={
        <FormActions
          disableSubmit={disableSubmit}
          onSubmit={onSubmit}
          onClose={onClose}
          cancelText={cancelText}
          submitText={submitText}
        />
      }
    >
      {data.map((formData) => (
        <TextField
          key={formData.name}
          onChange={onFieldChange}
          autoFocus
          margin="dense"
          rows={formData.type === 'textarea' ? 3 : 0}
          multiline={formData.type === 'textarea'}
          name={formData.name}
          label={formData.label}
          type={formData.type}
          value={formData.value}
          fullWidth
        />
      ))}
    </Dialog>
  );
}

export type IconProps = {
  color?: 'action' | 'primary' | 'error';
  size?: 'inherit' | 'small' | 'large';
};

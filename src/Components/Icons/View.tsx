import React from 'react';

import VisibilityIcon from '@material-ui/icons/Visibility';

import { IconProps } from './types';

export function View(props: IconProps) {
  const { color = 'action', size = 'inherit' } = props;

  return <VisibilityIcon color={color} fontSize={size} />;
}

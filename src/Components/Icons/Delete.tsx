import React from 'react';

import DeleteIcon from '@material-ui/icons/Delete';

import { IconProps } from './types';

export function Delete(props: IconProps) {
  const { color = 'action', size = 'inherit' } = props;

  return <DeleteIcon color={color} fontSize={size} />;
}

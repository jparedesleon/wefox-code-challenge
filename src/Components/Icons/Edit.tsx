import React from 'react';

import EditIcon from '@material-ui/icons/Edit';

import { IconProps } from './types';

export function Edit(props: IconProps) {
  const { color = 'action', size = 'inherit' } = props;

  return <EditIcon color={color} fontSize={size} />;
}

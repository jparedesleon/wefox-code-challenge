import React from 'react';

export function Logo() {
  return (
    <svg
      width="30"
      height="30"
      viewBox="0 0 400 400"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M200 0C130.033 0 66.6667 56.7167 66.6667 126.7C66.6667 196.667 124.483 280.2 200 400C275.517 280.2 333.333 196.667 333.333 126.7C333.333 56.7167 269.983 0 200 0ZM200 183.333C172.383 183.333 150 160.95 150 133.333C150 105.717 172.383 83.3333 200 83.3333C227.617 83.3333 250 105.717 250 133.333C250 160.95 227.617 183.333 200 183.333Z"
        fill="url(#paint0_linear)"
      />
      <defs>
        <linearGradient
          id="paint0_linear"
          x1="200"
          y1="0"
          x2="200"
          y2="400"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#DC255B" />
          <stop offset="1" stopColor="#F2743C" />
        </linearGradient>
      </defs>
    </svg>
  );
}

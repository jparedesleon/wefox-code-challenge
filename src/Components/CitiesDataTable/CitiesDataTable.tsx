import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { IconButton } from '@material-ui/core';

import { EditIcon, DeleteIcon, ViewIcon } from 'Components/Icons';
import { Cities, City } from 'Model';

type Props = {
  cities: Cities;
  onEditCity: (id: City['id']) => void;
  onViewCity: (id: City['id']) => void;
  onDeleteCity: (id: City['id']) => void;
};

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  margin: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

export function CitiesDataTable(props: Props) {
  const classes = useStyles();
  const { cities, onEditCity, onDeleteCity, onViewCity } = props;

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="center">Latitude</TableCell>
            <TableCell align="center">Longitude</TableCell>
            <TableCell align="center">Created</TableCell>
            <TableCell align="center">Updated</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {cities.map((city) => (
            <TableRow key={city.id}>
              <TableCell component="th" scope="row">
                {city.title}
              </TableCell>
              <TableCell align="center">{city.lat || '-'}</TableCell>
              <TableCell align="center">{city.long || '-'}</TableCell>
              <TableCell align="center">
                {city.createdAt?.toLocaleDateString() || '-'}
              </TableCell>
              <TableCell align="center">
                {city.updatedAt?.toLocaleDateString() || '-'}
              </TableCell>
              <TableCell align="center">
                <IconButton
                  aria-label="edit"
                  className={classes.margin}
                  onClick={() => {
                    onEditCity(city.id);
                  }}
                >
                  <EditIcon color="primary" />
                </IconButton>
                <IconButton
                  aria-label="view"
                  className={classes.margin}
                  onClick={() => {
                    onViewCity(city.id);
                  }}
                >
                  <ViewIcon />
                </IconButton>
                <IconButton
                  aria-label="delete"
                  className={classes.margin}
                  onClick={() => {
                    onDeleteCity(city.id);
                  }}
                >
                  <DeleteIcon color="error" />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

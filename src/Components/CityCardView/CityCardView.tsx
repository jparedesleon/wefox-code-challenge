import React from 'react';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import { Button } from 'Components/Button';
import { City } from 'Model';

type Props = {
  city: City;
  onClick: () => void;
};

const useStyles = makeStyles({
  root: {
    width: 400,
  },
  media: {
    height: 240,
  },
});

export function CityCardView(props: Props) {
  const classes = useStyles();
  const { city, onClick } = props;

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia className={classes.media} image={city?.imageUrl} title={city?.title} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {city?.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {city?.content}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button onClick={onClick} size="small" color="primary">
          Close
        </Button>
      </CardActions>
    </Card>
  );
}

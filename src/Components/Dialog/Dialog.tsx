import React from 'react';

import DialogMUI from '@material-ui/core/Dialog';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

type Props = {
  onClose: () => void;
  open: boolean;
  noWrap?: boolean;
  title?: string;
  actions?: React.ReactNode;
};

export function Dialog(props: React.PropsWithChildren<Props>) {
  const { open, onClose, children, title, actions, noWrap } = props;

  return (
    <DialogMUI open={open} onClose={onClose}>
      {title && <DialogTitle id="form-dialog-title">{title}</DialogTitle>}
      {noWrap ? children : <DialogContent>{children}</DialogContent>}
      {actions && <DialogActions>{actions}</DialogActions>}
    </DialogMUI>
  );
}

import React from 'react';

import { Paper } from '@material-ui/core';

import './Container.scss';

type Props = React.PropsWithChildren<{
  className?: string;
}>;

export function Container({ children, className }: Props) {
  const classes = `Container ${className}`;

  return (
    <div className={classes}>
      <Paper className="Container__paper" elevation={3}>
        {children}
      </Paper>
    </div>
  );
}

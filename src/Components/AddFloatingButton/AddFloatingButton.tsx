import React from 'react';

import AddIcon from '@material-ui/icons/Add';
import { Fab } from '@material-ui/core';

type Props = {
  onClick: () => void;
};

export function AddFloatingButton(props: Props) {
  const { onClick } = props;

  return (
    <Fab onClick={onClick} color="primary" aria-label="add">
      <AddIcon />
    </Fab>
  );
}

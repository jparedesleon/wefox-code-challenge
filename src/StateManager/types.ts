import * as citiesList from 'Controllers/CitiesListController';
import * as deleteCity from 'Controllers/DeleteCityController';
import * as createCity from 'Controllers/CreateCityController';
import * as editCity from 'Controllers/EditCityController';
import * as viewCity from 'Controllers/ViewCityController';

import { EmitSystemMessageAction } from './actions';

export type Dispatcher = (action: Actions) => void;

export type Action<T extends string, P = undefined> = {
  type: T;
  payload?: P;
};

export type State = {
  citiesListController: citiesList.CitiesListControllerState;
  deleteCityController: deleteCity.DeleteCityControllerState;
  createCityController: createCity.CreateCityControllerState;
  editCityController: editCity.EditCityControllerState;
  viewCityController: viewCity.ViewCityControllerState;
};

export type Actions =
  | EmitSystemMessageAction
  | citiesList.CitiesListControllerActions
  | createCity.CreateCityControllerActions
  | editCity.EditCityControllerActions
  | viewCity.ViewCityControllerActions
  | deleteCity.DeleteCityControllerActions;

export type StateContext = {
  state: State;
  dispatch: (a: Actions) => void;
};

export type SystemMessages =
  | citiesList.SystemMessages
  | createCity.SystemMessages
  | editCity.SystemMessages
  | deleteCity.SystemMessages;

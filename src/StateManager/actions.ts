import * as C from './constants';
import { Action, SystemMessages } from './types';

type EmitSystemMessagePayload = {
  message: SystemMessages;
  data?: unknown;
};
export type EmitSystemMessageAction = Action<typeof C.SYSTEM_MESSAGE, EmitSystemMessagePayload>;
export function emitSystemMessage(payload: EmitSystemMessagePayload): EmitSystemMessageAction {
  return {
    type: C.SYSTEM_MESSAGE,
    payload,
  };
}

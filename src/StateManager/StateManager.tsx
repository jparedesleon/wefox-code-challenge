import React, { useReducer, createContext } from 'react';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import * as citiesList from 'Controllers/CitiesListController';
import * as createCity from 'Controllers/CreateCityController';
import * as deleteCity from 'Controllers/DeleteCityController';
import * as editCity from 'Controllers/EditCityController';
import * as viewCity from 'Controllers/ViewCityController';

import * as T from './types';

let dispatcher: T.Dispatcher = () => {};

export const initialState: T.State = {
  citiesListController: citiesList.initialState,
  deleteCityController: deleteCity.initialState,
  createCityController: createCity.initialState,
  editCityController: editCity.initialState,
  viewCityController: viewCity.initialState,
};

export const StateContext = createContext<T.StateContext>({
  state: initialState,
  dispatch: dispatcher,
});

export const reducers = (state: T.State, action: T.Actions): T.State => {
  console.log('%cStateManager action:', 'color: red; font-weight: bold;', action, state);
  const combinedReducers = {
    citiesListController: citiesList.reducer(state.citiesListController, action),
    deleteCityController: deleteCity.reducer(state.deleteCityController, action),
    createCityController: createCity.reducer(state.createCityController, action),
    editCityController: editCity.reducer(state.editCityController, action),
    viewCityController: viewCity.reducer(state.viewCityController, action),
  };

  [
    ...deleteCity.middlewares,
    ...citiesList.middlewares,
    ...createCity.middlewares,
    ...editCity.middlewares,
    ...viewCity.middlewares,
  ].forEach((middleware) => middleware(action, dispatcher));

  return combinedReducers;
};

const theme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});

export function StateManager({ children }: React.PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer(reducers, initialState);

  if (dispatcher !== dispatch) {
    dispatcher = dispatch;
  }

  return (
    <StateContext.Provider value={{ state, dispatch }}>
      <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
    </StateContext.Provider>
  );
}
